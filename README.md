
![](https://bitbucket.org/laserllama/sourceoptics/raw/3d9517a1e803e181e7d85338f41b95b0cae9d20e/source_optics/static/logo_bg.png)

![](https://img.shields.io/badge/dynotherms-connected-blue) 
![](https://img.shields.io/badge/infracells-up-green) 
![](https://img.shields.io/badge/megathrusters-go-green)

Source Optics
=============

Source Optics helps explore code and team trends around (git) software projects.

More information, install instructions, and information about how to get involved 
at https://sourceoptics.io/

Authors
=======

SourceOptics is an ongoing project from Michael DeHaan <michael@michaeldehaan.net> 
and computer science students at NC State University.

License
=======

All source is provided under the Apache 2 license, (C) 2018-2019, All Project 
Contributors.

