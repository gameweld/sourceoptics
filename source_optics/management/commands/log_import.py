#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 17:10:58 2020

@author: jgwilli8
"""

from django.core.management.base import BaseCommand
from django.core.management import CommandError
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import DataError
from source_optics.models import (Log, Author, Organization)
import json
import sys
import traceback
import datetime
from django.utils import timezone


CURRENT_TZ = timezone.get_current_timezone()
LOG_DATE_FORMAT = '%Y-%m-%d'

            
def update_existing_log(existing_log, log_data):
    """Updates the log fields for an existing log given new data"""
    existing_log.author = log_data['author']
    existing_log.organization = log_data['organization']
    existing_log.date = log_data['date']
    existing_log.log_type = log_data['type']
    existing_log.hours = log_data['hours']
    existing_log.comments = log_data['comments']
    existing_log.save()


def save_new_log(log_data):
    """Adds new log to the DB"""
    Log.objects.create(log_id=log_data['log_id'], 
                            author=log_data['author'],
                            organization=log_data['organization'],
                            date=log_data['date'],
                            log_type=log_data['type'],
                            hours=log_data['hours'],
                            comments=log_data['comments']).save()


def check_log_exists(newlog_id):
    """Return the existing log if the log already exists in the DB"""
    try:
        #Check if the log exists
        return Log.objects.get(log_id=newlog_id)
    except:
        #else return nothing
        return
    

def validate_log_data(log):
    """Validates author, org, and date, throws exception if invalid"""
    #Validate log hours
    if (log['hours'] < 0):
        raise ValueError('Log hours \"{}\" cannot be negative'.format(log['hours']))
    
    #Validate author
    try:
        author_obj = Author.objects.get(email=log['author'])
    except:
        #Maybe the display name was given
        author_obj = Author.objects.get(display_name=log['author'])
    log['author'] = author_obj
    
    #Validate organization
    org_obj = Organization.objects.get(name=log['organization'])
    log['organization'] = org_obj
    
    #Validate date
    log_date = datetime.datetime.strptime(log['date'], LOG_DATE_FORMAT).replace(tzinfo=CURRENT_TZ)
    log['date'] = log_date
    
    #all fields now of correct type
    return log


def print_result(count, size, log_id, result_msg):
    if result_msg == 'FAILURE':
        sys.stderr.write('{}/{}: {} - ID: {}\n'.format(count, size, result_msg, log_id))
    else:
        print('{}/{}: {} - ID: {}'.format(count, size, result_msg, log_id))


def import_data(data):
    """Loops through logs to import"""
    size = len(data['logs'])
    count = 0
    total_successes = 0
    total_failures = 0
    result_msg = ''
    
    for log in data['logs']:
        count += 1
        try:
            #Check data and convert to correct types
            log_data = validate_log_data(log)
            #Check if the log exists
            existing_log = check_log_exists(log['log_id'])
            if not existing_log:
                #log doesn't exist
                save_new_log(log_data)
            else:
                #log does exist, need to update data
                update_existing_log(existing_log, log_data)
            result_msg = 'SUCCESS'
            total_successes += 1
        except ObjectDoesNotExist as e:
            result_msg = 'FAILURE'
            sys.stderr.write('Author or Organization does not exist\n')
            sys.stderr.write(str(log) + '\n')
            sys.stderr.write(traceback.format_exc() + '\n')
            total_failures += 1
        except ValueError as e:
            result_msg = 'FAILURE'
            sys.stderr.write(str(e) + '\n')
            sys.stderr.write(str(log) + '\n')
            sys.stderr.write(traceback.format_exc() + '\n')
            total_failures += 1
        except DataError as e:
            result_msg = 'FAILURE'
            sys.stderr.write('Could not save log\n')
            sys.stderr.write(str(log) + '\n')
            sys.stderr.write(traceback.format_exc() + '\n')
            total_failures += 1
        finally:
            print_result(count, size, log['log_id'], result_msg)
    
    print('Log import finished\nSuccess:{} Failure:{}'.format(total_successes, total_failures))


def import_from_file(file):
    """Imports logs from a json file"""
    print('Importing logs from file...')
    #Open file and load json data
    try:
        with open(file) as log_file:
            data = json.load(log_file)
            import_data(data)       
    except FileNotFoundError:
        raise CommandError('File not found')

class Command(BaseCommand):
    help = 'attempts to import a list of logs from a json file'
    
    def add_arguments(self, parser):
        parser.add_argument('-f', '--file',
                            dest='file',
                            type=str,
                            help='Indicate which file to import from',
                            default=None)
    
    
    def handle(self, *args, **kwargs):
        file = kwargs['file']
        if file == None:
            raise CommandError('No file provided')
        import_from_file(file)
            
            