# Copyright 2018-2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


from django.db import models
from django.db.models import Sum, Max, Min, Avg
from django.utils import timezone

# if author = null, entry represents total stats for interval
# if author = X, entry represent X's author stats for the given interval

class Statistic(models.Model):

    # below: these fields are used to expose what statistics show up in graph options
    # commenting out statistics removes them from the UI


    STATISTIC_FIELDS = {
        'Commit Total': 'commit_total',
        'Lines Added': 'lines_added',
        'Lines Changed': 'lines_changed',
        'Lines Removed': 'lines_removed',
        'Files Created': 'creates',
        'File Changes' : 'file_changes',
        'Days Active': 'days_active',
        'Average Commit Size': 'average_commit_size',
        'Commits Per Day': 'commits_per_day',
        # FIXME: these were experimental and may be removed if we create some other
        # metrics that better track refactoring
        # 'Bias': 'bias',
        # 'Flux': 'flux',
        'Commitment': 'commitment',
        'Earliest Commit Date': 'earliest_commit_date',
        'Latest Commit Date': 'latest_commit_date',
        'Days Since Seen': 'days_since_seen',
        'Days Before Joined': 'days_before_joined',
        'Longevity': 'longevity',
        'Moves': 'moves',
        # 'Edits': 'edits',

        # FIXME: Most log fields are temporarily disabled until we have a feature flag to enable logs
        'Log Days Active': 'log_days_active',
        'Total Log Hours': 'total_log_hours',
        'Log Hours Per Day': 'log_hours_per_day',
        'Earliest Log Date': 'earliest_log_date',
        'Latest Log Date': 'latest_log_date'
    }

    INTERVALS = (
        ('DY', 'Day'),
        ('WK', 'Week'),
        ('MN', 'Month'),
        ('LF', 'Lifetime')
    )

    start_date = models.DateTimeField(blank=False, null=True)
    interval = models.TextField(max_length=5, choices=INTERVALS)
    repo = models.ForeignKey('Repository', on_delete=models.CASCADE, null=True, related_name='repo')
    author = models.ForeignKey('Author', on_delete=models.CASCADE, blank=True, null=True, related_name='author')

    lines_added = models.IntegerField(blank = True, null = True)
    lines_removed = models.IntegerField(blank = True, null = True)
    lines_changed = models.IntegerField(blank = True, null = True)
    commit_total = models.IntegerField(blank = True, null = True)
    file_changes = models.IntegerField(blank = True, null = True)
    author_total = models.IntegerField(blank = True, null = True)
    days_active = models.IntegerField(blank=True, null=True, default=0)
    average_commit_size = models.IntegerField(blank=True, null=True, default=0)

    commits_per_day = models.FloatField(blank=True, null=True, default=0)
    bias = models.IntegerField(blank=True, null=True, default=0)
    commitment = models.FloatField(blank=True, null=True, default=0)

    earliest_commit_date = models.DateTimeField(blank=True, null=True)
    latest_commit_date = models.DateTimeField(blank=True, null=True)
    days_since_seen = models.IntegerField(blank=False, null=True, default=-1)
    days_before_joined = models.IntegerField(blank=False, null=True, default=-1)
    longevity = models.IntegerField(blank=True, null=True, default=0)
    last_scanned = models.DateTimeField(blank=True, null=True)

    moves = models.IntegerField(blank=True, null=True, default=0)
    edits = models.IntegerField(blank=True, null=True, default=0)
    creates = models.IntegerField(blank=True, null=True, default=0)

    log_days_active = models.IntegerField(blank=True, null=True, default=0)
    total_log_hours = models.FloatField(blank=True, null=True, default=0.0)
    log_hours_per_day = models.FloatField(blank=True, null=True, default=0)
    earliest_log_date = models.DateTimeField(blank=True, null=True)
    latest_log_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        if self.author is None:
            return "Stat(Total): I=" + str(self.interval[0]) + " D=" + str(self.start_date)
        else:
            return "Stat(Author): " + str(self.author) + " I=" + str(self.interval[0]) + " D=" + str(self.start_date)

    class Meta:

        unique_together = [
            [ 'start_date', 'interval', 'repo', 'author' ]
        ]

        indexes = [
            models.Index(fields=['start_date', 'interval', 'repo', 'author'], name='author_rollup3'),
        ]

    @classmethod
    def aggregate_data(cls, queryset):
        return queryset.aggregate(
            lines_added=Sum("lines_added"),
            lines_removed=Sum("lines_removed"),
            lines_changed=Sum("lines_changed"),
            commit_total=Sum("commit_total"),
            days_active=Sum("days_active"),
            moves=Sum("moves"),
            edits=Sum("edits"),
            creates=Sum("creates"),
            # as a result, we won't show it in graphs for now. Consider removing.
            file_changes=Sum("file_changes"),
            average_commit_size=Avg("average_commit_size"),
            commits_per_day=Avg("commits_per_day"),
            bias=Avg("bias"),
            commitment=Avg("commitment"),
            earliest_commit_date=Min("earliest_commit_date"),
            latest_commit_date=Max("latest_commit_date"),
            days_since_seen=Max("days_since_seen"), #Probably wrong agg method
            days_before_joined=Max("days_before_joined"), #Probably wrong too
            longevity=Sum("longevity"),
            log_days_active=Sum("log_days_active"),
            total_log_hours=Sum("total_log_hours"),
            log_hours_per_day=Avg("log_hours_per_day"),
            earliest_log_date=Min("earliest_log_date"),
            latest_log_date=Max("latest_log_date")
        )

    @classmethod
    def annotate(cls, queryset):
        """
        This method is for all of the statistics that are displayed on the Stats page
        """
        # FIXME: eventually make all this model driven so it doesn't need double entry
        return queryset.annotate(
            # this next one is kind of weird, but we need it to add the value in
            annotated_repo=Max("repo__name"),
            annotated_author_name=Max("author__display_name"),
            annotated_last_scanned=Max("last_scanned"),
            annotated_lines_added=Sum("lines_added"),
            annotated_lines_removed=Sum("lines_removed"),
            annotated_lines_changed=Sum("lines_changed"),
            annotated_commit_total=Sum("commit_total"),
            annotated_days_active=Sum("days_active"),
            annotated_earliest_commit_date=Min("earliest_commit_date"),
            annotated_latest_commit_date=Max("latest_commit_date"),
            annotated_total_log_hours=Sum("total_log_hours"),
        )

    @classmethod
    def annotate_for_punchcard(cls, queryset):
        # FIXME: eventually make all this model driven so it doesn't need double entry
        return queryset.annotate(
            # this next one is kind of weird, but we need it to add the value in
            annotated_repo=Max("repo__name"),
            annotated_author_name=Max("author__display_name"),
            annotated_lines_added=Sum("lines_added"),
            annotated_lines_removed=Sum("lines_removed"),
            annotated_lines_changed=Sum("lines_changed"),
            annotated_commit_total=Sum("commit_total"),
            annotated_author_id = Max("author_id")
        )

    @classmethod
    def compute_interval_statistic(cls, queryset, interval=interval, repo=None, author=None, start=None, end=None, for_update=False):

        from . statistic import Statistic
        from . author import Author
        from . file_change import FileChange

        # used by rollup.py to derive a rollup interval from another.

        if interval != 'LF':
            assert start is not None
            assert end is not None
        assert repo is not None

        data = Statistic.aggregate_data(queryset)
        author_count = Author.author_count(repo, start=start, end=end)

        if author and isinstance(author, int):
            # FIXME: find where this is happening and make sure the system returns objects.
            author = Author.objects.get(pk=author)

        stat = Statistic(
            start_date=start,
            interval=interval,
            repo=repo,
            author=author,
            lines_added=data['lines_added'],
            lines_removed=data['lines_removed'],
            lines_changed=data['lines_changed'],
            commit_total=data['commit_total'],
            days_active=data['days_active'],
            file_changes=data['file_changes'],
            author_total=author_count,
            moves=data['moves'],
            creates=data['creates'],
            edits=data['edits'],
            log_days_active=data['log_days_active'],
            total_log_hours=data['total_log_hours']
        )

        stat.compute_derived_values()

        today = timezone.now()

        if for_update and interval == 'LF':

            all_earliest = repo.earliest_commit_date()
            all_latest = repo.latest_commit_date()
            stat.earliest_commit_date = repo.earliest_commit_date(author)
            stat.latest_commit_date = repo.latest_commit_date(author)
            stat.days_since_seen = (all_latest - stat.latest_commit_date).days
            stat.days_before_joined = (stat.earliest_commit_date - all_earliest).days
            stat.longevity = (stat.latest_commit_date - stat.earliest_commit_date).days + 1
            stat.commitment = (stat.days_active / (1 + stat.longevity))
            stat.earliest_log_date = repo.organization.earliest_log_date(author)
            stat.latest_log_date = repo.organization.latest_log_date(author)
            stat.last_scanned = today

            update_stats = None
            if author:
                update_stats = Statistic.objects.filter(repo=repo, author=author)
            else:
                update_stats = Statistic.objects.filter(repo=repo, author__isnull=True)
            # these stats are somewhat denormalized 'globals' but allows us to include them efficiently in time-series tooltips which is nice.
            update_stats.update(
                earliest_commit_date = stat.earliest_commit_date,
                latest_commit_date = stat.latest_commit_date,
                #latest_commit_date = stat.latest_commit_date,
                days_since_seen = stat.days_since_seen,
                days_before_joined = stat.days_before_joined,
                longevity = stat.longevity,
                commitment = stat.commitment,
                earliest_log_date = stat.earliest_log_date,
                latest_log_date = stat.latest_log_date,
                last_scanned = today,
            )

        elif not for_update and queryset.count():

            if author:
                first = queryset.first()
                stat.earliest_commit_date = first.earliest_commit_date
                stat.latest_commit_date = first.latest_commit_date
                stat.days_since_seen = first.days_since_seen
                stat.days_before_joined = first.days_before_joined
                stat.longevity = first.longevity
                stat.commitment = first.commitment
                stat.earliest_log_date = first.earliest_log_date
                stat.latest_log_date = first.latest_log_date
                stat.last_scanned = today
            else:
                # leave these at defaults
                pass

        return stat

    def compute_derived_values(self):
        self.average_commit_size = Statistic._div_safe(self, 'lines_changed', 'commit_total')
        self.commits_per_day = Statistic._div_safe(self, 'commit_total', 'days_active')
        self.log_hours_per_day = Statistic._div_safe(self, 'total_log_hours', 'log_days_active')
        if self.lines_added and self.lines_removed:
            self.bias = self.lines_added - self.lines_removed
        else:
            self.bias = 0

    @classmethod
    def _div_safe(cls, data, left, right):
        # FIXME: more of a question - these are float fields, do we want this, or should we make them int fields?
        # we're casting anyway at this point.
        if isinstance(data, dict):
            # FIXME: when done refactoring, I would think we'd only have objects, and this part
            # would no longer be needed.
            if data[right]:
                return int(float(data[left]) / float(data[right]))
            else:
                return 0
        else:
            left  = getattr(data, left, None)
            right = getattr(data, right, None)
            if left and right:
                return int(float(left) / float(right))
            else:
                return 0

    def copy_fields_for_update(self, other):
        # TODO: make this list of fields gathered automatically from the model so maintaince
        # of this function isn't required?  Get all Int + Date fields, basically
        self.lines_added = other.lines_added
        self.lines_removed = other.lines_removed
        self.lines_changed = other.lines_changed
        self.commit_total = other.commit_total
        self.file_changes = other.file_changes
        self.author_total = other.author_total
        self.earliest_commit_date = other.earliest_commit_date
        self.latest_commit_date = other.latest_commit_date
        self.days_since_seen = other.days_since_seen
        self.days_before_joined = other.days_before_joined
        self.days_active = other.days_active
        self.average_commit_size = other.average_commit_size
        self.commits_per_day = other.commits_per_day
        self.bias = other.bias
        self.commitment = other.commitment
        self.longevity = other.longevity
        self.moves = other.moves
        self.creates = other.creates
        self.edits = other.edits
        self.log_days_active = other.log_days_active
        self.total_log_hours = other.total_log_hours
        self.log_hours_per_day = other.log_hours_per_day
        self.earliest_log_date = other.earliest_log_date
        self.latest_log_date = other.latest_log_date


    @classmethod
    def queryset_for_range(cls, repos=None, authors=None, interval=None, author=None, start=None, end=None):

        assert repos or authors
        assert interval is not None

        stats = Statistic.objects.select_related('author', 'repo')

        if authors:
            stats = stats.filter(author__pk__in=authors)
        else:
            stats = stats.filter(author__isnull=True)

        if repos:
            stats = stats.filter(repo__pk__in=repos)

        if start and interval != 'LF':
            stats = stats.filter(start_date__range=(start,end), interval=interval)
        else:
            stats = stats.filter(interval='LF')

        return stats

    def to_dict(self):
        # FIXME: this really should take the interval as a parameter, such that it can not
        # supply statistics that don't make sense if the interval != lifetime ('LF').
        result = dict(
            days_active=self.days_active,
            commit_total=self.commit_total,
            average_commit_size=self.average_commit_size,
            lines_changed=self.lines_changed,
            lines_added=self.lines_added,
            lines_removed=self.lines_removed,
            longevity=self.longevity,
            earliest_commit_date=str(self.earliest_commit_date),
            latest_commit_date=str(self.latest_commit_date),
            days_before_joined=self.days_before_joined,
            days_since_seen=self.days_since_seen,
            author_total=self.author_total,
            file_changes=self.file_changes,
            commits_per_day=self.commits_per_day,
            bias=self.bias,
            commitment=self.commitment,
            creates=self.creates,
            edits=self.edits,
            moves=self.moves,
            log_days_active=self.log_days_active,
            total_log_hours=self.total_log_hours,
            log_hours_per_day=self.log_hours_per_day,
            earliest_log_date=str(self.earliest_log_date),
            latest_log_date=str(self.latest_log_date)
        )
        if self.author:
            result['author']=self.author.email
        else:
            result['author']=None
        return result
