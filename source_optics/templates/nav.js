$(document).ready(function() {

var navBar =`
<button type="button" id="homeButton" class="btn btn-outline-secondary btn-sm">Home &gt;</button>
{% if org %}
<button type="button" id="reposButton" class="btn btn-outline-secondary btn-sm">Org: {{ org.name }} &gt;</button>
{% endif %}
{% if repo and not multiple_repos_selected %}
<button type="button" id="repoButton" class="btn btn-outline-secondary btn-sm">Repo: {{ repo.name }} &gt;</button>
{% endif %}
{% if author %}
<button type="button" id="authorButton" class="btn btn-outline-secondary btn-sm">Author: {{ author.email }} &gt;</button>
{% endif %}
&nbsp;
&nbsp;
{% if repo or repos_str or author %}
{% if not author %}
{% comment %}
author graphs across all repos are coming soon
{% endcomment %}
<button type="button" id="dataButton" class="btn btn-outline-info btn-sm">Graphs</button>
{% endif %}
{% if not multiple_repos_selected %}
<button type="button" id="statsButton" class="btn btn-outline-info btn-sm">Stats</button>
<button type="button" id="feedButton" class="btn btn-outline-info btn-sm">Commit Feed</button>
<button type="button" id="logsButton" class="btn btn-outline-info btn-sm">Logs</button>
<button type="button" id="punchcardButton" class="btn btn-outline-info btn-sm">Punchcard</button>
<button type="button" id="filesButton" class="btn btn-outline-info btn-sm">Files</button>

{% endif %}
{% endif %}
{% if org and not repo and not author %}
<button type="button" id="dataExploreSelectedButton" class="btn btn-outline-info btn-sm">Graph Selected</button>
{% endif %}
`;

$('#navBar').html(navBar);
$('#homeButton').click(function() {
   window.location.href = "/";
});

{% if org %}

  $('#reposButton').click(function() { window.location.href = "/org/{{ org.pk }}/repos"; });

{% endif %}

{% if author %}

  $('#authorButton').click(function() { window.location.href = "/author/{{ author.pk }}"; });
  $('#dataButton').click(function()  { window.location.href = "/graph2?author={{ author.pk }}&start={{ start_str }}&end={{ end_str }}&xaxis={{ xaxis }}&yaxis={{ yaxis }}&zaxis={{ zaxis }}&graph_type={{ graph_type }}"; });
  $('#feedButton').click(function()   { window.location.href = "/report/commits?author={{ author.pk }}&start={{ start_str}}&end={{ end_str }}"; });
  $('#statsButton').click(function()  { window.location.href = "/report/stats?author={{ author.pk }}&start={{ start_str}}&end={{ end_str }}"; });
  $('#logsButton').click(function()  { window.location.href = "/report/log_details?author={{ author.pk }}&start={{ start_str}}&end={{ end_str }}"; });

{% elif repo and not multiple_repos_selected %}

  $('#repoButton').click(function()   { window.location.href = "/repo/{{ repo.pk }}?start={{ start_str }}&end={{ end_str }}"; });
  $('#feedButton').click(function()   { window.location.href = "/report/commits?repo={{ repo.pk }}&start={{ start_str}}&end={{ end_str }}"; });
  $('#dataButton').click(function()  { window.location.href = "/graph2?repo={{ repo.pk }}&start={{ start_str }}&end={{ end_str }}&xaxis={{ xaxis }}&yaxis={{ yaxis }}&zaxis={{ zaxis }}&graph_type={{ graph_type }}"; });
  $('#statsButton').click(function()  { window.location.href = "/report/stats?repo={{ repo.pk }}&start={{ start_str}}&end={{ end_str }}"; });
  $('#filesButton').click(function()  { window.location.href = "/report/files?repo={{ repo.pk }}&start={{ start_str}}&end={{ end_str }}&path="; });
  $('#punchcardButton').click(function()  { window.location.href = "/report/punchcard?repo={{ repo.pk }}&start={{ start_str}}&end={{ end_str }}"; });
  $('#logsButton').click(function()  { window.location.href = "/report/logs?repo={{ repo.pk }}&start={{ start_str}}&end={{ end_str }}"; });

{% elif repos_str %}

  $('#dataButton').click(function() { window.location.href = "/graph2?repos={{ repos_str }}&start={{ start_str}}&end={{ end_str }}&xaxis={{ xaxis }}&yaxis={{ yaxis }}&zaxis={{ zaxis }}&graph_type={{ graph_type }}"; });


{% endif %}

{% if org and not repo and not author %}
  $('#dataExploreSelectedButton').click(function() {
      var repos = [];
      $.each($("input[name='repo']:checked"), function(){
          repos.push($(this).val());
      });
      var repos_str = repos.join("+");
      var new_url = "/graph2?org={{ org.pk }}&repos=" + repos_str + "&start={{ start_str }}&end={{ end_str }}&xaxis={{ xaxis }}&yaxis={{ yaxis }}&zaxis={{ zaxis }}&graph_type={{ graph_type }}";
      if (repos.length) {
          window.location.href = new_url
      }
  });
{% endif %}

{% comment %}
FIXME: disable the button instead and grey-out. DRY.
{% endcomment %}
{% if mode == 'stats' %}
   var text = $('#statsButton').text()
   $('#statsButton').text("*" + text)
{% elif mode == 'feed' %}
   var text = $('#feedButton').text()
   $('#feedButton').text("*" + text)
{% elif mode == 'orgs' %}
   var text = $('#orgsButton').text()
   $('#orgsButton').text("*" + text )
{% elif mode == 'repos' %}
   var text = $('#reposButton').text()
   $('#reposButton').text("*" + text)
{% elif mode == 'author' %}
   var text = $('#authorButton').text()
   $('#authorButton').text("*" + text)
{% elif mode == 'repo' %}
   var text = $('#repoButton').text()
   $('#repoButton').text("*" + text)
{% elif mode == 'files' %}
   var text = $('#filesButton').text()
   $('#filesButton').text("*" + text)
{% elif mode == 'punchcard' %}
   var text = $('#punchcardButton').text()
   $('#punchcardButton').text("*" + text)
{% elif mode == 'data' %}
   var text = $('#dataButton').text()
   $('#dataButton').text("*" + text)
{% elif mode == 'logs' %}
   var text = $('#logsButton').text()
   $('#logsButton').text("*" + text)
{% endif %}


});