function authorNav(value) {
   var index = "<a href='/author/" + value + "' aria-label='User Home'><i aria-hidden='true' class='fas fa-user' title='Index'></i></a>&nbsp;";
   var data = "<a href='/graph2?author=" + value + "&repos=ALL&start={{ start_str }}&end={{ end_str }}&xaxis={{ xaxis }}&yaxis={{ yaxis }}&zaxis={{ zaxis }}&graph_type={{ graph_type }}'><i class='fas fa-chart-line'></i></a>&nbsp;";
   var stats  = "<a href='/report/stats?author=" + value + "&start={{ start_str }}&end={{ end_str }}' aria-label='Statistics'><i aria-hidden='true' class='fas fa-table' title='Stats'></i></a>&nbsp;";
   var commit_feed = "<a href='/report/commits?author=" + value + "&start={{ start_str }}&end={{ end_str }}' aria-label='Commits'><i aria-hidden='true' class='fas fa-rss-square' title='Commits'></i></a>&nbsp;";
   <!--
   var log_feed = "<a href='/report/log_details?author=" + value + "&start={{ start_str }}&end={{ end_str }}' aria-label='Log View'><i aria-hidden='true' class='fas fa-book' title='Log View'></i></a>&nbsp;";
   -->
   var log_feed = "";
   return index + data + stats + commit_feed + log_feed;
}

function repoNav(value) {
   var index = "<a href='/repo/" + value + "?start={{ start_str }}&end={{ end_str }}' aria-label='Repo Home'><i aria-hidden='true' class='fas fa-database' title='Index'></i></a>&nbsp;";
   var data = "<a href='/graph2?repo=" + value + "&start={{ start_str }}&end={{ end_str }}&xaxis={{ xaxis }}&yaxis={{ yaxis }}&zaxis={{ zaxis }}&graph_type={{ graph_type }}' aria-label='Files'><i aria-hidden='true' class='fas fa-chart-line' title='Graphs'></i></a>&nbsp;";
   var stats  = "<a href='/report/stats?repo=" + value + "&start={{ start_str }}&end={{ end_str }}' aria-label='Statistics'><i aria-hidden='true' class='fas fa-table' title='Stats'></i></a>&nbsp;";
   var commit_feed = "<a href='/report/commits?repo=" + value + "&start={{ start_str }}&end={{ end_str }}' aria-label='Commits'><i aria-hidden='true' class='fas fa-rss-square' title='Commits'></i></a>&nbsp;";
   <!--
   var log_feed = "<a href='/report/logs?repo=" + value + "&start={{ start_str }}&end={{ end_str }}&path=' aria-label='Log View'><i aria-hidden='true' class='fas fa-book' title='Log View'></i></a>&nbsp;";
   -->
   var log_feed = "";
   var files = "<a href='/report/files?repo=" + value + "&start={{ start_str }}&end={{ end_str }}&path=' aria-label='Files'><i aria-hidden='true' class='fas fa-folder-open' title='Files'></i></a>&nbsp;";
   var punchcard = "<a href='/report/punchcard?repo=" + value + "&start={{ start_str }}&end={{ end_str }}' aria-label='Punchcard'><i aria-hidden='true' class='far fa-id-card' title='Punchcard'></i></a>&nbsp;";
   return index + data + stats + commit_feed + log_feed + punchcard + files;
}
