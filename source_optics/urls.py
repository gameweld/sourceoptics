# Copyright 2018-2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from django.urls import include, path

# REST Framework is temporarily disabled until Django/filter-library compatibility can
# be sorted out. It will also need authentication if we add auth to the rest of the app.

# from rest_framework import routers

from source_optics.views import api
from source_optics.views import views

# api_router = router = routers.DefaultRouter()

# router.register(r'users', api.UserViewSet)
# router.register(r'groups', api.GroupViewSet)
# router.register(r'repositories', api.RepositoryViewSet)
# router.register(r'organizations', api.OrganizationViewSet)
# router.register(r'credentials', api.CredentialViewSet)
# router.register(r'author', api.AuthorViewSet)
# router.register(r'statistic', api.StatisticViewSet)
# router.register(r'commit', api.CommitViewSet)

urlpatterns = [

    # top level pages - object hierarchy navigation
    path('', views.list_orgs, name='list_orgs'),

    # FIXME: use org in query string, else show all repos
    path('org/<org>/repos', views.list_repos, name='list_repos'),

    path('author/<author>', views.author_index, name='author_index'),

    path('repo/<repo>', views.repo_index, name='repo_index'),
    path('pies', views.pie_charts, name='pie_charts'),

    path('graph/path_segment', views.graph_path_segment, name='graph_path_segment'),

    # REPORTS AND GRAPH PAGES - RATHER FLEXIBLE BY QUERY STRING
    path('report/stats', views.report_stats, name='report_author_stats'),
    path('report/commits', views.report_commits, name='report_commits'),
    path('report/files', views.report_files, name='report_files'),
    path('report/punchcard', views.report_punchcard, name='report_punchcard'),
    path('graph2', views.graph_page, name='graph_page'),
    path('graph2/custom', views.graph_custom, name='graph_custom'),
    path('report/logs', views.report_logs, name='report_logs'),
    path('report/log_details', views.report_log_details, name='report_log_details'),

    # REST API
    # path('api/', include(api_router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # Webhooks
    path('webhook', views.webhook_post, name='webhook_post'),
    path('webhook/', views.webhook_post, name='webhook_post'),

]
