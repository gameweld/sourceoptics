# Copyright 2018-2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# api.py - Django REST Framework REST code

from django.contrib.auth.models import Group, User
#from django_filters.rest_framework import DjangoFilterBackend

# disabled for compat reasons (see comment in urls.py)
# from rest_framework import viewsets

from source_optics.models import (Author, Commit, Credential,
                                  Organization, Repository, Statistic)

# from source_optics.serializers import (AuthorSerializer, CommitSerializer,
#                                       CredentialSerializer, GroupSerializer,
#                                       OrganizationSerializer,
#                                       RepositorySerializer,
#                                       StatisticSerializer, UserSerializer)


#class UserViewSet(viewsets.ReadOnlyModelViewSet):
#    """
#    API endpoint that allows users to be viewed or edited.
#    """
#    queryset = User.objects.all().order_by('-date_joined')
#    serializer_class = UserSerializer
#    filter_backends = (DjangoFilterBackend,)


#class GroupViewSet(viewsets.ReadOnlyModelViewSet):
#    """
#    API endpoint that allows groups to be viewed or edited.
#    """
#    queryset = Group.objects.all()
#    serializer_class = GroupSerializer
#    filter_backends = (DjangoFilterBackend,)


#class RepositoryViewSet(viewsets.ReadOnlyModelViewSet):
#    queryset = Repository.objects.all()
#    serializer_class = RepositorySerializer
#    filter_backends = (DjangoFilterBackend,)
#    filterset_fields = ('name', 'url', 'tags', 'last_scanned', 'enabled', 'organization')

#class OrganizationViewSet(viewsets.ReadOnlyModelViewSet):
#    queryset = Organization.objects.all()
#    serializer_class = OrganizationSerializer
#    filter_backends = (DjangoFilterBackend,)
#    filterset_fields = ('name',)

#class CredentialViewSet(viewsets.ReadOnlyModelViewSet):
#    queryset = Credential.objects.all()
#    serializer_class = CredentialSerializer
#    filter_backends = (DjangoFilterBackend,)
#    filterset_fields = ('name', 'username')

#class CommitViewSet(viewsets.ReadOnlyModelViewSet):
#    queryset = Commit.objects.all()
#    serializer_class = CommitSerializer
#    filter_backends = (DjangoFilterBackend,)
#    filterset_fields = ('repo', 'author', 'sha', 'commit_date', 'author_date', 'subject')

#class AuthorViewSet(viewsets.ReadOnlyModelViewSet):
#    queryset = Author.objects.all()
#    serializer_class = AuthorSerializer
#    filter_backends = (DjangoFilterBackend,)
#    filterset_fields = ('email',)

# FIXME: all the current statistic fields aren't here, we should read this from the Statistic model
# # so we don't forget when adding new fields

#class StatisticViewSet(viewsets.ReadOnlyModelViewSet):
#    queryset = Statistic.objects.all()
#    serializer_class = StatisticSerializer
#    filter_backends = (DjangoFilterBackend,)
#     filterset_fields = ('start_date', 'interval', 'repo', 'author')
