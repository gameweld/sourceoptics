# Copyright 2018-2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# graphs.py - generate altair graphs as HTML snippets given panda dataframe inputs (see dataframes.py).  These are loaded
# as URL fragments, each filling in DIVs.

import functools
from .. models import Statistic, FileChange
from django.db.models import Sum

import random
import string
import altair as alt
from django import template
import json
from django.shortcuts import redirect, render
from django.http import HttpResponse
from . import dataframes, reports
from . import graphs as graph_module
from .scope import Scope
import pandas as pd
import sys

from plotly.offline import plot
import plotly.graph_objs as go
from plotly.subplots import make_subplots

# tootltips for graphs that have been partitioned by author
AUTHOR_TIME_SERIES_TOOLTIPS = ['day','author','commit_total', 'lines_changed', 'files_changes', 'longevity', 'days_since_seen' ]

# tooltip fields for basic graphs (no partitions)
TIME_SERIES_TOOLTIPS = ['day','commit_total', 'lines_changed', 'files_changed', 'author_total' ]

# FIXME: add tooltips for graphs that were partitioned by repo?

# javascript needed to wrap an altair chart
TEMPLATE_CHART = """
<div id="{output_div}"></div>
    <script type="text/javascript">
    var spec = {spec};
    var embed_opt = {embed_opt};
    function showError({output_div}, error){{
        {output_div}.innerHTML = ('<div class="error">'
                        + '<p>JavaScript Error: ' + error.message + '</p>'
                        + "<p>This usually means there's a typo in your chart specification. "
                        + "See the javascript console for the full traceback.</p>"
                        + '</div>');
        throw error;
    }}
    const {output_div} = document.getElementById('{output_div}');
    vegaEmbed("#{output_div}", spec, embed_opt)
      .catch(error => showError({output_div}, error));
    </script>
"""

GRAPH_FONT = dict(
        family='Helvetica Neue',
        size=14,
        color='#000000',
        )

MARKER_2D_LINE = 4
MARKER_3D_LINE = 4
MARKER_2D_SCATTER = 10
MARKER_3D_SCATTER = 4

# FIXME: move more of these views into settings, and have defaults that are upgrade-safe.

PIE_FIELDS = ['Commit Total', 'Lines Changed', 'File Changes', 'Total Log Hours']

def render_chart(chart):
    """
    wraps an altair chart ("chart") with the required javascript/html to display that chart
    """
    spec = chart.to_dict()
    output_div = '_' + ''.join(random.choices(string.ascii_letters + string.digits, k=7))
    embed_opt = {"mode": "vega-lite", "actions": False}
    c = template.Context()
    return template.Template(TEMPLATE_CHART.format(output_div=output_div, spec=json.dumps(spec), embed_opt=json.dumps(embed_opt))).render(c)

@functools.lru_cache(maxsize=64)
def get_stat(repo, author, start, end, aspect):
   """
   Used when building a legend, this looks up a statistic for a given author to determine the sort order.
   FIXME: Realistically, we should be passing this in from dataframes.py code and keeping the visualization layer dumb.
   """

   value = Statistic.objects.filter(
       author=author,
       repo=repo,
       interval='DY',
       start_date__range=(start,end)
   ).aggregate(
       lines_changed=Sum('lines_changed'),
       commit_total=Sum('commit_total')
   )[aspect]

   if value is None:
       return -10000

   return value

def time_plot(scope=None, df=None, repo=None, y=None, by_author=False, top=None, aspect=None):
    """
    Generates a time series area plot from a dataframe.  Nearly all graphs go through here.
    FIXME: the chart type should be a parameter - this will not always return altair charts.

    :param df: a pandas dataframe
    :param y: the name of the y axis from the dataframe
    :param top: the legend, used for the top authors plot sorting, as a list of authors
    :param aspect: the aspect the chart was limited by
    :param author: true if the chart is going to be showing authors vs the whole team together
    :return: chart HTML
    """
    assert df is not None
    assert scope is not None
    start = scope.start
    end = scope.end
    repo = scope.repo

    if top:
        assert start is not None
        assert end is not None
        assert repo is not None
        # sort top authors by the statistic we filtered them by
        top = reversed(sorted(top, key=lambda x: get_stat(repo, x, start, end, aspect)))
        top = [ x.get_display_name() for x in top ]
        top.append('OTHER')

    tooltips = TIME_SERIES_TOOLTIPS
    if by_author:
        tooltips = AUTHOR_TIME_SERIES_TOOLTIPS
    if scope.author:
        tooltips.append('repo')

    alt.data_transformers.disable_max_rows()

    if by_author:
        df = df.sort_values(by=['author'])
        chart = alt.Chart(df, height=600, width=600).mark_area( opacity=0.9).encode(
            x=alt.X('date:T', axis = alt.Axis(title = 'date', format = ("%b %Y"))),
            y=alt.Y(y, scale=alt.Scale(zero=True)),
            color=alt.Color('author', sort=top),
            tooltip=tooltips
        ).interactive()
    elif not scope.multiple_repos_selected():
        chart = alt.Chart(df, height=600, width=600).mark_line().encode(
            x=alt.X('date:T', axis = alt.Axis(title = 'date', format = ("%b %Y"))),
            y=alt.Y(y, scale=alt.Scale(zero=True)),
            tooltip=tooltips
        ).interactive()
    else:
        # multiple repos
        chart = alt.Chart(df, height=600, width=600).mark_line().encode(
            x=alt.X('date:T', axis = alt.Axis(title = 'date', format = ("%b %Y"))),
            y=alt.Y(y, scale=alt.Scale(zero=True)),
            color=alt.Color('repo'),
            tooltip=tooltips
        ).interactive()

    return render_chart(chart)

def path_segment_plot(df, scope, top_authors):
    """
    Path segment charts work somewhat differently as they don't use Statistics objects. There's a bit more duplicated
    code that still needs to find the right shape and home yet (FIXME).
    """

    assert top_authors is not None

    def get_author_stat(repo, author):

        value = FileChange.objects.filter(
            commit__author=author,
            commit__repo=scope.repo,
            commit__commit_date__range=(scope.start, scope.end),
            file__path=scope.path
        )
        if scope.file:
            value = value.filter(file__name=scope.file)
        value = value.count()
        if value is None:
            return -10000
        return value


    # sort top authors by the statistic we filtered them by
    top = reversed(sorted(top_authors, key=lambda x: get_author_stat(scope, x)))

    top = [ x.get_display_name() for x in top ]
    top.append('OTHER')


    # a basic plot of directory activity that can be improved later as we choose to update it.
    tooltips = ['date:T','commits','author']
    chart = alt.Chart(df, height=150, width=300).mark_area().encode(
        x=alt.X('date:T', axis=alt.Axis(title='date', format=("%b %Y")), scale=alt.Scale(zero=False, clamp=True)),
        y=alt.Y('commits', scale=alt.Scale(zero=True)),
        color=alt.Color('author'), # sort=top),
        tooltip=tooltips
    ).interactive()
    return render_chart(chart)

# FIXME: lots of dead code here from old graphs views, remove...

def get_basic_plot(scope, df, y):
    """
    A basic plot draws a graph for the whole repo, and is not segmented by author or repo.
    """
    return graph_module.time_plot(df=df, scope=scope, y=y, top=None)

def get_advanced_plot(scope, df, top, y, aspect1='repo', aspect2='commit_total'):
    """
    Advanced plots are segmented by the author and repo, depending on context.
    """
    if scope.multiple_repos_selected():
        return graph_module.time_plot(df=df, scope=scope, y=y, top=top, aspect=aspect1)
    else:
        return graph_module.time_plot(df=df, scope=scope, y=y, top=top, by_author=True, aspect=aspect2)

def render_basic_plot(request, y):
    """
    View-level code: Renders a basic line chart from a request.
    """
    scope = Scope(request)
    (df, _) = dataframes.get_basic_dataframe(scope)
    scope.context['graph'] = get_basic_plot(scope, df, y=y)
    return render(request, 'graph.html', context=scope.context)

def render_advanced_plot(request, y):
    """
    View-level code: Renders a stacked area chart segmented by repo or user, based on context.
    """
    scope = Scope(request)
    (df, top) = dataframes.get_advanced_dataframe(scope)
    scope.context['graph'] = get_advanced_plot(scope, df, top, y=y)
    return render(request, 'graph.html', context=scope.context)

def sort_top(scope, top):
    # FIXME: there's a bug here where we try to sort based on the y axis - the value usually won't be available
    # so instead of 'scope.yaxis' use 'commit_total'.
    top = reversed(sorted(top, key=lambda x: get_stat(scope.repo, x, scope.start, scope.end, 'commit_total')))
    top = [x for x in top]
    return top

def render_plotly_graph(request, scope):
    """
    Get a plotly graph for data explorer page
    """
    #Check that axes were given. There is some code in scope that sets axes to None if bad axes were given.
    if scope.xaxis == None:
        return HttpResponse(status=500)
    
    #Get dataframe
    (df, top) = dataframes.get_dataframe(scope)
    if not scope.repos and (scope.graph_type == 'Line' or scope.graph_type == 'Stacked'):
        top = sort_top(scope, top)
    
    #Get graph with given type
    if scope.graph_type == 'Line':
        assert(scope.xaxis == 'Time')
        if scope.zaxis:
            figure = get_3d_lineplot(scope, df, top)
        else:
            figure = get_2d_lineplot(scope, df, top)
    elif scope.graph_type == 'Stacked':
        assert(scope.xaxis == 'Time')
        figure = get_2d_stack(scope, df, top)
    elif scope.graph_type == 'Scatter':
        if scope.zaxis:
            if (scope.xaxis == 'Time'):
                figure = get_3d_lineplot(scope, df, top, symbol='markers', marker_size=MARKER_3D_SCATTER)
            else:
                figure = get_3d_scatterplot(scope, df)
        else:
            if scope.xaxis == 'Time':
                figure = get_2d_lineplot(scope, df, top, symbol='markers', marker_size=MARKER_2D_SCATTER)
            else:
                figure = get_2d_scatterplot(scope, df)
    elif scope.graph_type == 'Mesh':
        assert(scope.xaxis != 'Time')
        assert(scope.zaxis)
        figure = get_3d_mesh(scope, df)
    else:
        return HttpResponse(status=500)
    
    #Wrap graph in div tags
    plt_div = plot(figure, output_type='div')
    return HttpResponse(plt_div)
    

def get_2d_scatterplot(scope, df):
    """
    Generate a 2d scatterplot
    """
    title = "{} vs {}".format(scope.xaxis_label, scope.yaxis_label)
    
    if scope.repos:
        repositories = scope.repos
    else:
        repositories = [scope.repo]
    
    
    fig = go.Figure()
    for repo in repositories:
        repo_df = df.loc[df['repo'] == repo.name]
        x_list = repo_df[scope.xaxis].tolist()
        y_list = repo_df[scope.yaxis].tolist()
        traces = repo_df['author'].tolist()
        fig.add_trace(go.Scatter(
            x = x_list,
            y = y_list,
            text=traces,
            hovertemplate = 'Repo: ' + repo.name + '<br>Author: ' + '%{text}<br>' + scope.xaxis_label + ': %{x}<br>'+ scope.yaxis_label + ': %{y}<br>',
            mode = 'markers',
            name = repo.name,
            showlegend = True,
            marker_size=MARKER_2D_SCATTER
            ))
    
    fig.update_layout(
        title=title,
        xaxis_title=scope.xaxis_label,
        yaxis_title=scope.yaxis_label,
        font=GRAPH_FONT
        )
    return fig

def get_3d_scatterplot(scope, df):
    """
    Generate a 3d scatterplot
    """
    title = "{} vs {} vs {}".format(scope.xaxis_label, scope.yaxis_label, scope.zaxis_label)
    
    if scope.repos:
        repositories = scope.repos
    else:
        repositories = [scope.repo]
    
    
    fig = go.Figure()
    for repo in repositories:
        repo_df = df.loc[df['repo'] == repo.name]
        x_list = repo_df[scope.xaxis].tolist()
        y_list = repo_df[scope.yaxis].tolist()
        z_list = repo_df[scope.zaxis].tolist()
        traces = repo_df['author'].tolist()
        fig.add_trace(go.Scatter3d(
            x = x_list,
            y = y_list,
            z = z_list,
            text=traces,
            hovertemplate = 'Repo: ' + repo.name + '<br>Author: ' + '%{text}<br>' + scope.xaxis_label + ': %{x}<br>'+ scope.yaxis_label + ': %{y}<br>' + scope.zaxis_label + ': %{z}',
            mode = 'markers',
            name = repo.name,
            showlegend = True,
            marker_size=MARKER_3D_SCATTER
            ))
    
    fig.update_layout(
        title=title,
        scene = dict(xaxis_title=scope.xaxis_label, yaxis_title=scope.yaxis_label, zaxis_title=scope.zaxis_label),
        font=GRAPH_FONT
        )
    return fig


def get_2d_lineplot(scope, df, top, symbol='lines+markers', marker_size=MARKER_2D_LINE):
    """
    Generate a 2d lineplot
    """
    if scope.repos:
        col_key = 'repo'
        entities = scope.repos
        title = "Time vs {}".format(scope.yaxis_label)
        hover_prefix = 'Repo: '
    else:
        col_key = 'author'
        entities = top + ['OTHER']
        title = "{}: Time vs {}".format(scope.repo, scope.yaxis_label)
        hover_prefix = 'Author: '
    
    fig = go.Figure()
    for ent in entities:
        if scope.repos:
            name = ent.name
        elif ent != 'OTHER':
            name = ent.display_name
        else:
            name = 'OTHER'
        ent_df = df.loc[df[col_key] == name]
        ent_df = ent_df.sort_values(by=['date'])
        fig.add_trace(go.Scatter(
                x=ent_df['date'],
                y=ent_df[scope.yaxis],
                hovertemplate = hover_prefix + name + '<br>Time: %{x}<br>'+ scope.yaxis_label + ': %{y}<br>',
                mode=symbol,
                marker_size=marker_size,
                name=name,
                showlegend = True))
    
    fig.update_layout(
        title=title,
        xaxis_title=scope.xaxis_label,
        yaxis_title=scope.yaxis_label,
        font=GRAPH_FONT
        )
    return fig

def get_2d_stack(scope, df, top, symbol='lines'):
    """
    Generate a 2d stacked lineplot
    """
    if scope.repos:
        col_key = 'repo'
        entities = scope.repos
        title = "Time vs {}".format(scope.yaxis_label)
        hover_prefix = 'Repo: '
    else:
        col_key = 'author'
        entities = top + ['OTHER']
        title = "{}: Time vs {}".format(scope.repo, scope.yaxis_label)
        hover_prefix = 'Author: '
    
    fig = go.Figure()
    for ent in reversed(entities):
        if scope.repos:
            name = ent.name
        elif ent != 'OTHER':
            name = ent.display_name
        else:
            name = 'OTHER'
        ent_df = df.loc[df[col_key] == name]
        ent_df = ent_df.sort_values(by=['date'])
        fig.add_trace(go.Scatter(
                x=ent_df['date'],
                y=ent_df[scope.yaxis],
                hovertemplate = hover_prefix + name + '<br>Time: %{x}<br>'+ scope.yaxis_label + ': %{y}<br>',
                mode=symbol,
                name=name,
                showlegend = True,
                stackgroup='one')),
    
    fig.update_layout(
        title=title,
        xaxis_title=scope.xaxis_label,
        yaxis_title=scope.yaxis_label,
        font=GRAPH_FONT
        )
    return fig


def get_3d_lineplot(scope, df, top, symbol='lines', marker_size=MARKER_3D_LINE):
    """
    Generate a 3d lineplot
    """
    if scope.repos:
        col_key = 'repo'
        entities = scope.repos
        title = "Time vs {} vs {}".format(scope.yaxis_label, scope.zaxis_label)
        hover_prefix = 'Repo: '
    else:
        col_key = 'author'
        entities = top + ['OTHER']
        title = "{}: Time vs {} vs {}".format(scope.repo, scope.yaxis_label, scope.zaxis_label)
        hover_prefix = 'Author: '
    
    fig = go.Figure()
    for ent in entities:
        if scope.repos:
            name = ent.name
        elif ent != 'OTHER':
            name = ent.display_name
        else:
            name = 'OTHER'
        auth_df = df.loc[df[col_key] == name]
        auth_df = auth_df.sort_values(by=['date'])
        fig.add_trace(go.Scatter3d(
                x=auth_df['date'],
                y=auth_df[scope.yaxis],
                z=auth_df[scope.zaxis],
                hovertemplate = hover_prefix + name + '<br>Time: %{x}<br>'+ scope.yaxis_label + ': %{y}<br>' + scope.zaxis_label + ': %{z}<br>',
                mode=symbol,
                marker_size=marker_size,
                name=name,
                showlegend = True))
    fig.update_layout(
        title=title,
        scene = dict(xaxis_title=scope.xaxis_label, yaxis_title=scope.yaxis_label, zaxis_title=scope.zaxis_label),
        font=GRAPH_FONT,
        #autosize=True,
        #height=2000,
        )
    return fig


def get_3d_mesh(scope, df):
    """
    Generate a 3d mesh graph
    """
    df = sort_df(df, scope.yaxis)
    x_list = df[scope.xaxis].tolist()
    y_list = df[scope.yaxis].tolist()
    z_list = df[scope.zaxis].tolist()
    
    if (scope.repos):
        traces = df['repo'].tolist()
        title = "{} vs {} vs {}".format(scope.xaxis_label, scope.yaxis_label, scope.zaxis_label)
    else:
        traces = df['author'].tolist()
        title = "{}: {} vs {} vs {}".format(scope.repo, scope.xaxis_label, scope.yaxis_label, scope.zaxis_label)
    
    for trace in traces:
        fig = go.Figure(data=[
                go.Mesh3d(
                        x=x_list,
                        y=y_list,
                        z=z_list,
                        hovertemplate = scope.xaxis_label + ': %{x}<br>'+ scope.yaxis_label + ': %{y}<br>' + scope.zaxis_label + ': %{z}<br>',
                        name=""
                        )
                ])
    fig.update_layout(
        title=title,
        scene = dict(xaxis_title=scope.xaxis_label, yaxis_title=scope.yaxis_label, zaxis_title=scope.zaxis_label),
        font=GRAPH_FONT
        )
    return fig

def sort_df(df, axis):
    return df.sort_values(axis, ascending=False)

def get_pie_charts(scope):

    # see if we have any logs

    field_choices = PIE_FIELDS

    fields = [Statistic.STATISTIC_FIELDS[field] for field in field_choices]
    df = dataframes.get_lifetime_author_dataframe(scope, fields=fields, top_auths=20)

    # most projects in SourceOptics won't have log data, so don't show this column if there isn't any
    # FIXME: re-enable once feature flag is set...
    #  total_log_hours = df.loc[df.author == 'total', 'total_log_hours'].tolist()[0]
    #if total_log_hours <= 0:
    #    field_choices = PIE_FIELDS_NO_LOGS
    #    fields = [Statistic.STATISTIC_FIELDS[field] for field in field_choices]

    fig = make_subplots(1, len(fields), specs=[[{'type':'domain'}] * len(fields)], subplot_titles=field_choices)
    (labels, values_dict) = consolidate_values(df, fields)

    for i in range(len(fields)):
        values = values_dict[fields[i]]


        fig.add_trace(go.Pie(
                labels=labels,
                values=values,
                 name="",
                 hovertemplate="Author: %{label}<br>Total: %{value}<br>Percentage: %{percent}",
        ), 1, i+1)
    fig.update_traces(textposition='inside')
    fig.update_layout(
            uniformtext_minsize=8,
            uniformtext_mode='hide',
            font=GRAPH_FONT,
            )
    
    plt_div = plot(fig, output_type='div')
    return HttpResponse(plt_div)

def consolidate_values(df, fields):
    #'total' will be last in df
    labels = []
    values_dict = {}
    author_df = df.loc[df['author'] != 'total']
    total_df = df.loc[df['author'] == 'total']
    
    for field in fields:
        values_dict[field] = []
    for index, row in df.iterrows():
        if row['author'] != 'total':
            for field in fields:
                if values_dict[field] is not None:
                    values_dict[field].append(row[field])
                else:
                    values_dict[field].append(0)
            labels.append(row['author'])
    
    if not total_df.empty:
        labels.append('OTHER')
        for field in fields:
            if row[field] is not None:
                val = total_df.iloc[0][field]
            else:
                val = 0
            for index, row in author_df.iterrows():
                if row[field] is not None:
                    val -= row[field]
            values_dict[field].append(val)
    
    return (labels, values_dict)
    
    