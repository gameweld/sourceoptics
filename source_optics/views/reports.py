
import json, copy
from source_optics.models import (Repository, Statistic, Commit, Author, File, FileChange, Log)
from django.core.paginator import Paginator, EmptyPage
from django.db.models import Sum

def files(scope):
    # the files report view
    repo = scope.repo
    path = scope.path
    if path == '/':
        path = ''

    kids = get_child_paths(scope, repo, path)
    files = get_files(scope, repo, path)

    if path == "":
        path = "/"

    return dict(
        path=path,
        browse_paths=kids,
        paths_length=len(kids),
        browse_files=files,
        files_length=len(files)
    )

def get_child_paths(scope, repo, path):

    # find all the directory paths
    all_paths = FileChange.objects.filter(
        commit__repo=repo,
        file__path__startswith=path,
        commit__commit_date__range=(scope.start, scope.end)
    ).values_list('file__path', flat=True).distinct().all()

    slashes = path.count('/')
    desired = slashes

    # find all the paths that are one level deeper than the specified path
    # the removal of "=>" deals with moved path detection in old versions of the program w/ legacy data
    children = [ dict(path=path) for path in sorted(all_paths) if ((not '=>' in path) and (path.count('/') == desired)) ]

    return children

def get_files(scope, repo, path):

    all_files = FileChange.objects.filter(
        commit__repo=repo,
        file__path=path,
        commit__commit_date__range=(scope.start, scope.end)
    ).values_list('file', flat=True).distinct().order_by('file__name').all()

    all_files = File.objects.filter(pk__in=all_files)

    return [ dict(filename=f.name, path=f.path) for f in all_files.all() ]

def get_page(objs, scope):
    """
    Helper method for commits_feed, logs_feed, and commit_log_summary that gets the
    current page based on the objects and the size of the page. The catching of the 
    exception is so if there are two different types of data on the page (for example
    commits and logs), and there are more pages for commits than logs, the page will
    display the last page for logs while continuing to move through more pages for commits
    """
    paginator = Paginator(objs, scope.page_size)
    try:
        page = paginator.page(scope.page)
    except EmptyPage as e:
        page = paginator.get_page(scope.page)
    return page

def commit_filter(repo, author, organization, start, end, scope):
    """
    Helper method for commits_feed and commit_log_summary that filters commit objects
    by repo & author, repo, author, or organization (in that order) and then filtered further by date
    """
    # FIXME: this looks like a method we should add to Scope() but only to be called when needed
    if repo and author:
        objs = Commit.objects.filter(repo=repo, author=author)
    elif repo:
        objs = Commit.objects.filter(repo=repo)
    elif author:
        objs = Commit.objects.filter(author=author)
    elif organization:
        objs = Commit.objects.filter(repo__organization=organization)
    else:
        raise NotImplementedError("Unexpected filter options for commit_filter")
    if start and end:
        objs = objs.filter(commit_date__range=(start,end))
    
    if scope.path:
        objs = objs.filter(file_changes__file__path=scope.path)
    if scope.file:
        objs = objs.filter(file_changes__file__name=scope.file)
    if scope.extension:
        objs = objs.filter(file_changes__file__ext=scope.extension)

    # all this nested filtering apparently can make bad queries, so we should probably unroll all of the above?

    objs = objs.select_related('author').order_by('-commit_date')
    return objs

def commits_feed(scope):
    objs = None

    repo = scope.repo
    author = scope.author
    organization = scope.org
    start = scope.start
    end = scope.end

    objs = commit_filter(repo, author, organization, start, end, scope)
    count = objs.count()
    page = get_page(objs, scope)

    results = []
    # we may wish to show filechange info here
    for commit in page:
        desc = commit.subject
        if desc is None:
            desc = ""
        desc = desc[:255]
        results.append(dict(
            repo=commit.repo.name,
            commit_date=str(commit.commit_date),
            author_id=commit.author.pk,
            author=commit.author.email,
            author_name=commit.author.display_name,
            sha=commit.sha,
            subject=desc
        ))

    return dict(results=results, page=page, count=count)

def log_filter(repo, author, organization, start, end):
    """
    A helper method for logs_feed and commit_log_summary
    Returns a list of log objects filtered by author, organization, and date range
    """

    # FIXME: this looks like a method we should add to Scope() but only to be called when needed
    if author:
        objs = Log.objects.filter(author=author)
    elif organization:
        objs = Log.objects.filter(organization=organization)
    else:
        raise NotImplementedError("Unexpected filter options for log_filter")
    if start and end:
        objs = objs.filter(date__range=(start,end))
    
    # # all this nested filtering apparently can make bad queries, so we should probably unroll all of the above?

    objs = objs.select_related('author').order_by('-date')
    return objs

def logs_feed(scope):
    """
    The version used for the log details page (author-specific)
    Returns a dictionary with fields results, page, and count
    results - An array of log objects for the current page (dicts)
    page - a pagination-related object returned using our collection of logs sent to django's paginator
    count - total count of log objects across all pages
    """
    objs = None

    repo = scope.repo
    author = scope.author
    organization = scope.org
    start = scope.start
    end = scope.end

    objs = log_filter(repo, author, organization, start, end)
    count = objs.count()
    page = get_page(objs, scope)

    results = []
    # we may wish to show filechange info here
    for log in page:
        desc = log.comments
        if desc is None:
            desc = ""
        # TODO determine if description should be appended like the desc for commits was
        #desc = desc[:255]
        results.append(dict(
            org=log.organization.name,
            log_date=str(log.date),
            log_type=log.log_type,
            hours=log.hours,
            author_id=log.author.pk,
            author=log.author.email,
            author_name=log.author.display_name,
            comments=desc
        ))

    return dict(results=results, page=page, count=count)

def commit_log_summary(scope):
    """
    This method provides commit and log summary objects 
    (custom object with author, commit summary, log summary)
    to views for display in log page table
    Returns dict of results of these author-commit-log summary objects, page data, and total count
    """
    objs = None

    repo = scope.repo
    author = scope.author
    organization = scope.org
    start = scope.start
    end = scope.end

    # first get commit objs
    commit_objs = commit_filter(repo, author, organization, start, end, scope)
    # then get a list of the authors we need to aggregate logs for too.
    # Use the collection of commits to get these authors
    author_ids = set(commit_objs.values_list('author', flat=True))
    author_objects = Author.objects.filter(pk__in=author_ids)
    author_objects = author_objects.order_by('display_name')

    page = get_page(author_objects, scope)

    count = 0
    results = []
    # at this point page is defined too; all fields to return are defined,
    # but results still needs to be defined

    for indiv_author in page:
        count = count + 1 # for row count
        # make a copy of the scope so we can use it in methods without modifying it here
        temp_scope = copy.copy(scope)
        # create a dictionary representation of the author's commit-log summary and append to results
        indiv_author_summary = process_summary_for_author(indiv_author, temp_scope, commit_objs)
        results.append(indiv_author_summary)

    assert scope.author == None # making sure our temp scope didn't make unwanted changes
    return dict(results=results, page=page, count=count)

def process_summary_for_author(indiv_author, scope, commit_objs):
    '''
    A helper method for processing commit-log summaries for each author
    indiv_author - an Author object to process a commit-log summary for
    scope - a scope object
    commit_objs - a query set of commits to filter through for the author
    '''
    # get the author's commits
    author_commits = commit_objs.filter(author=indiv_author) # just go through the commits we already singled out
    author_commit_count = author_commits.count() # and count the author's commits too

    # get the file count for commits for author
    scope.author = indiv_author # TEMPORARY - to be reset before method returns
    # from log_details - TODO look into refactoring into method if permanent solution
    stats_data = author_stats_table(scope)
    file_count = get_num_files(stats_data)
    
    # get the author's logs and related count + hour count data
    author_logs = log_filter(scope.repo, indiv_author, scope.org, scope.start, scope.end)
    author_log_count = author_logs.count()
    if author_log_count > 0:
        hour_count_num = author_logs.aggregate(hour_count=Sum('hours')).get('hour_count')
        # This is formatting the hour count so that it only has two decimal places showing in the string
        hour_count = f'{hour_count_num:.2f}'
    else:
        hour_count = '0.00'
    
    # wrapping the author's data all together
    return dict(
        author_id=indiv_author.id,
        author=indiv_author.display_name,
        commit_summary="%s commits in %s files" % (author_commit_count, file_count),
        log_summary="%s logs for %s hours" % (author_log_count, hour_count)
    )

def get_num_files(stats_data):
    """
    Calculates the number of file change events an author has created, edited, or moved for the commits
    header.
    """
    # FIXME: can we stop using this?
    if len(stats_data) == 1:
        num_files = stats_data[0]['files_changes']
    else:
        num_files = 0
    return num_files

def _annotations_to_table(stats, primary, lookup):

    """
    Processes a list of statistics objects and converts annotated values
    from those statistics objects back to the field names used in the tables.
    """

    results = []

    for entry in stats:
        new_item = {}
        new_item[primary] = entry[lookup]
        for (k,v) in entry.items():
            if k.startswith("annotated_"):
                k2 = k.replace("annotated_","")
                if 'date' in k2 or 'last_scanned' in k2 or 'name' in k2:
                    new_item[k2] = str(v)
                else:
                    new_item[k2] = v
        # for making things easy with ag-grid, the primary key is available multiple times:
        for x in [ 'details1', 'details2', 'details3']:
            new_item[x] = entry[lookup]

        # FIXME: I don't like this is happening here in a non-generic way, but ah well for now, we should
        # move this to a method in the stats class like "non_annotated_stats" or something?
        # print("DEBUG, entry: %s" % entry)
        latest_date = entry['annotated_latest_commit_date']
        earliest_date = entry['annotated_earliest_commit_date']
        if(latest_date and earliest_date):
            new_item["longevity"] = (latest_date - earliest_date).days
        else:
            new_item["longevity"] = 0

        results.append(new_item)
    return results


def author_stats_table(scope, limit=None):
    """
    this drives the author tables, both ranged and non-ranged, accessed off the main repo list.
    the interval 'LF' shows lifetime stats, but elsewhere we just do daily roundups, so this parameter
    should really be a boolean.  The limit parameter is not yet used.
    """

    # FIXME: this performs one query PER author and could be rewritten to be a LOT more intelligent.

    (repos, authors) = scope.standardize_repos_and_authors()
    interval = 'DY'
    stats = Statistic.queryset_for_range(repos=repos, authors=authors, start=scope.start, end=scope.end, interval=interval)
    data = None
    if not scope.author:
        stats = Statistic.annotate(stats.values('author__email')).order_by('author__email')
        data = _annotations_to_table(stats, 'author', 'author__email')
    else:
        stats = Statistic.annotate(stats.values('repo__name')).order_by('repo__name')
        data = _annotations_to_table(stats, 'repo', 'repo__name')
    return data


# FIXME: see what's up with the author table

def author_stats_punchcard(scope):
    """
    Aggregates statistics about authors and the number of commits they've made
    each day so that a punchcard can be displayed.
    """

    # FIXME: this performs one query PER author and could be rewritten to be a LOT more intelligent.

    (repos, authors) = scope.standardize_repos_and_authors()
    interval = 'DY'
    stats = Statistic.queryset_for_range(repos=repos, authors=authors, start=scope.start, end=scope.end, interval=interval).values()
    stats = Statistic.annotate_for_punchcard(stats.values('author__email')).order_by('author__email')

    paginator = Paginator(stats, scope.page_size)
    page = paginator.page(scope.page)

    results = []

    for entry in page:
        # each entry is an author
        new_entry = dict()
        new_entry['author_email'] = entry['author__email']
        new_entry['author_name'] = entry['annotated_author_name']
        # these stats are TOTALS for the author
        new_entry['lines_added'] = entry['annotated_lines_added']
        new_entry['lines_removed'] = entry['annotated_lines_removed']
        new_entry['lines_changed'] = entry['annotated_lines_changed']
        new_entry['commits'] = []
        new_entry['logs'] = []
        # now for commit data
        commits = [commit for commit in Commit.objects.filter(
            author=entry['annotated_author_id'], repo=scope.repo, commit_date__range=(scope.start, scope.end)).annotate(
                lines_added_sum=Sum('file_changes__lines_added'),lines_removed_sum=Sum('file_changes__lines_removed')).values()]
        for commit in commits:
            relevant_commit_data = helper_annotate_commit(commit)
            new_entry['commits'].append(relevant_commit_data)
        # end commit data
        # begin log data
        logs = [log for log in Log.objects.filter(author=entry['annotated_author_id'], organization=scope.org, date__range=(scope.start, scope.end)).values()]
        for log in logs:
            # making this a dict to make it consistent with treatment of commits and to be more flexible in the future
            relevant_log_data = dict()
            relevant_log_data['log_date'] = str(log['date'])
            relevant_log_data['log_hours'] = log['hours']
            new_entry['logs'].append(relevant_log_data)
        # end log data
        results.append(new_entry)

    return [entry for entry in results]

def helper_annotate_commit(commit):
    '''
    Params: commit is a dictionary representation of a commit object
    Returns: A dictionary from the data in that commit, including its date
        as well as the # of lines added, removed, and changed for that commit.
    '''
    # these are put into individual dicts with the commit_date so that the lines added/removed/changed is kept with its associated date
    relevant_commit_data = dict()
    relevant_commit_data['commit_date'] = str(commit['commit_date'])
    if(commit['lines_added_sum']):
        relevant_commit_data['lines_added'] = commit['lines_added_sum']
    else:
        relevant_commit_data['lines_added'] = 0
    if(commit['lines_removed_sum']):
        relevant_commit_data['lines_removed'] = commit['lines_removed_sum']
    else:
        relevant_commit_data['lines_removed'] = 0
    relevant_commit_data['lines_changed'] = relevant_commit_data['lines_added'] + relevant_commit_data['lines_removed']
    return relevant_commit_data

def repo_table(scope): # repos, start, end):

    #this drives the list of all repos within an organization, showing the statistics for them within the selected
    #time range, along with navigation links.

    (repos, authors) = scope.standardize_repos_and_authors()
    interval = 'DY'
    # FIXME: explain
    repos = [ x.pk for x in scope.available_repos.all() ]
    stats = Statistic.queryset_for_range(repos=repos, authors=authors, start=scope.start, end=scope.end, interval=interval)
    stats = Statistic.annotate(stats.values('repo__name')).order_by('repo__name')
    data = _annotations_to_table(stats, 'repo', 'repo__name')

    # FIXME: insert in author count, which is ... complicated ... this can be optimized later
    # we should be able to grab every repo and annotate it with the author count in one extra query tops
    # but it might require manually writing it.
    for d in data:
        repo = d['repo']
        author_count = Author.author_count(repo, start=scope.start, end=scope.end)
        d['author_count'] = author_count

    # some repos won't have been scanned, and this requires a second query to fill them into the table
    repos = Repository.objects.filter(last_scanned=None, organization=scope.org)
    for unscanned in repos:
        data.append(dict(
            repo=unscanned.name
        ))
    return data


def orgs_table(scope):

    results = []
    for org in scope.orgs:
        row = dict()
        row['name'] = org.name
        row['repo_count'] = org.repos.count()
        row['details1'] = org.pk
        results.append(row)
    return json.dumps(results)
