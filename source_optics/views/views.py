# Copyright 2018-2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# views.py - code immediately behind all of the web routes.  Renders pages, graphs, and charts.

# simplejson is needed because log_hours are of type Decimal which is not JSON serializable
import simplejson as json
import traceback
from urllib.parse import parse_qs
from django.core import serializers
from django.http import HttpResponse, HttpResponseServerError
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt
from source_optics.views.webhooks import Webhooks
from django.core.exceptions import ObjectDoesNotExist
from . import dataframes, reports
from . import graphs as graphs
from ..models import Statistic
from .scope import Scope

# =================================================================================================================
# GRAPH FRAGMENTS

def graph_path_segment(request):
    """
    Activity graph for an individual file or path segment.
    """
    scope = Scope(request)
    top_authors = dataframes.top_authors_for_path(scope)
    df = dataframes.path_segment_series(scope, top_authors)
    scope.context['graph'] = graphs.path_segment_plot(df=df, scope=scope, top_authors=top_authors)
    return render(request, 'graph.html', context=scope.context)

def graph_page(request):
    """
    Information needed for the graph page
    """
    scope = Scope(request)

    # FIXME: load these from settings but have user configurable defaults
    # FIXME: this should use model names not statistics.py translations to avoid typos & future translation challenges?

    favorite_presets = {
            "Commits" : [ "Time", "Commit Total"],
            "Commits vs days active": ["Commit Total", "Days Active"],
            "Line impact (3D)": ["Lines Added", "Lines Changed", "Lines Removed"],
            }
    axis_options = Statistic.STATISTIC_FIELDS.keys
    scope.context.update(dict(
        title = "SourceOptics: Graphs",
        mode = 'data',
        favorites = favorite_presets,
        axis = axis_options
    ))
    return render(request, 'data_explorer.html', context=scope.context)

def graph_custom(request):
    """
    Actually get the plotly graph
    """
    scope = Scope(request)
    return graphs.render_plotly_graph(request, scope)

def pie_charts(request):
    scope = Scope(request)
    return graphs.get_pie_charts(scope)

# =================================================================================================================
# OBJECT TRAVERSAL PAGES

def list_orgs(request):
    """
    List all organizations in the app.
    """
    scope = Scope(request, add_orgs_table=True)
    scope.context.update(dict(
        title = "Source Optics: index",
        mode = 'orgs'
    ))
    return render(request, 'orgs.html', context=scope.context)

def list_repos(request, org=None):
    """
    List all repositories for an org.
    """
    scope = Scope(request, org=org, add_repo_table=True)
    scope.context.update(dict(
        title = "Source Optics: %s organization" % scope.org.name,
        mode = 'repos'
    ))
    return render(request, 'repos.html', context=scope.context)

def repo_index(request, repo=None):
    """
    Show the homepage summary for a repo, which mostly links to graphs/stats.
    """
    scope = Scope(request, repo=repo)
    scope.context['mode'] = 'repo'
    return render(request, 'repo.html', context=scope.context)

def author_index(request, author=None):
    """
    Show the homepage for an author which mostly links to graphs/stats.
    """
    scope = Scope(request, author=author)
    scope.context['mode'] = 'author'
    return render(request, 'author.html', context=scope.context)

# =================================================================================================================
# DETAIL PAGES

def show_graphs(request):
    """
    For a given author OR repo, show all relevant graphs
    """
    scope = Scope(request)
    scope.context['mode'] = 'graphs'
    return render(request, 'graphs.html', context=scope.context)

def report_stats(request):
    """
    generates a partial graph which is loaded in the repo graphs page. more comments in graphs.py
    """
    scope = Scope(request)
    data = reports.author_stats_table(scope)

    scope.context.update(dict(
        title = "SourceOptics: stats view",
        author_count = len(data),
        table_json = json.dumps(data),
        mode = 'stats',
    ))

    # FIXME: should be repo_authors ? perhaps this will be standardized...
    return render(request, 'stats.html', context=scope.context)

def report_punchcard(request):
    """
    generates a partial graph which is loaded in the repo graphs page. more comments in graphs.py
    """
    scope = Scope(request)
    data = reports.author_stats_punchcard(scope)
    scope.context.update(dict(
        title = "SourceOptics: Punchcard View",
        author_count = len(data),
        punchcard_data = json.dumps(data),
        mode = 'punchcard',
        punch = '<svg style="background: red" width="100" height="100"></svg>'
    ))

    #if scope.context.authors: 
    #print(scope.context)
   # else: 
    #    print('not found')

    return render(request, 'punchcard.html', context=scope.context)

def report_commits(request, org=None):
    # FIXME: how about a scope object?
    scope = Scope(request)
    data = reports.commits_feed(scope)
    page = data['page']

    assert scope.repo or scope.author

    # FIXME: this needs cleanup - move generic pagination support to a common function
    # TODO: title can come from commits_feed function.  Right now nothing else
    # needs immediate pagination so this can wait.

    scope.context.update(dict(
        title = "Source Optics: commit feed",
        table_json = json.dumps(data['results']),
        page_number = page.number,
        has_prev = page.has_previous(),
        mode = "feed"
    ))

    if scope.context['has_prev']:
        if scope.repo:
            scope.context['prev_link'] = "/report/commits?repo=%s&start=%s&end=%s&page=%s" % (scope.repo.pk, scope.start_str, scope.end_str, page.previous_page_number())
        elif scope.author:
            scope.context['prev_link'] = "/report/commits?author=%s&start=%s&end=%s&page=%s" % (scope.author.pk, scope.start_str, scope.end_str, page.previous_page_number())
    scope.context['has_next'] = page.has_next()
    if scope.context['has_next']:
        if scope.repo:
            scope.context['next_link'] = "/report/commits?repo=%s&start=%s&end=%s&page=%s" % (scope.repo.pk, scope.start_str, scope.end_str, page.next_page_number())
        elif scope.author:
            scope.context['next_link'] = "/report/commits?author=%s&start=%s&end=%s&page=%s" % (scope.author.pk, scope.start_str, scope.end_str, page.next_page_number())

    return render(request, 'commits.html', context=scope.context)

def report_files(request):
    """
    generates a browseable directory tree
    """
    scope = Scope(request)
    data = reports.files(scope)
    scope.context.update(dict(
        title = "SourceOptics: tree view",
        mode = 'files',
        browse_files = data['browse_files'],
        browse_paths = data['browse_paths']
    ))
    return render(request, 'files.html', context=scope.context)

def report_logs(request, org=None):
    # adapted from report_commits
    # FIXME: how about a scope object?
    scope = Scope(request)
    data = reports.commit_log_summary(scope)
    page = data['page']

    scope.context.update(dict(
        title = "Source Optics: Commit and Log View",
        table_json = json.dumps(data['results']),
        page_number = page.number,
        has_prev = page.has_previous(),
        mode = "logs"
    ))

    scope.context['has_prev'] = page.has_previous()
    if scope.context['has_prev']:
        if scope.repo:
            scope.context['prev_link'] = "/report/logs?repo=%s&start=%s&end=%s&page=%s" % (scope.repo.pk, scope.start_str, scope.end_str, page.previous_page_number())
    scope.context['has_next'] = page.has_next()
    if scope.context['has_next']:
        if scope.repo:
            scope.context['next_link'] = "/report/logs?repo=%s&start=%s&end=%s&page=%s" % (scope.repo.pk, scope.start_str, scope.end_str, page.next_page_number())
    
    return render(request, 'logs.html', context=scope.context)

def report_log_details(request, org=None):
    """
    Updates information for the commits and logs details page. Shows aggregate values
    in the headers and commits and logs for a given author.
    """
    # FIXME: how about a scope object?
    scope = Scope(request)
    commit_data = reports.commits_feed(scope)
    log_data = reports.logs_feed(scope)
    stats_data = reports.author_stats_table(scope)

    page = select_page(commit_data, log_data)
    num_files = reports.get_num_files(stats_data)
    commit_header = ("Commits (%d Commits in %d Files)" % (commit_data['count'], num_files)) 
    log_hours = get_log_hours(log_data['results'])
    log_count = log_data['count']
    log_header = f'Logs ({log_count} Logs for {log_hours:.2f} Hours)'

    scope.context.update(dict(
        title = "Source Optics: Commit and Log Detailed View",
        commit_header = commit_header,
        commits_table_json = json.dumps(commit_data['results']),
        log_header = log_header,
        logs_table_json = json.dumps(log_data['results']),
        page_number = page.number,
        has_prev = page.has_previous(),
        mode = "logs"
    ))

    if scope.context['has_prev']:
        if scope.author:
            scope.context['prev_link'] = "/report/log_details?author=%s&start=%s&end=%s&page=%s" % (scope.author.pk, scope.start_str, scope.end_str, page.previous_page_number())
    scope.context['has_next'] = page.has_next()
    if scope.context['has_next']:
        if scope.author:
            scope.context['next_link'] = "/report/log_details?author=%s&start=%s&end=%s&page=%s" % (scope.author.pk, scope.start_str, scope.end_str, page.next_page_number())

    return render(request, 'log_details.html', context=scope.context)

def select_page(commit_data, log_data):
    """
    Selects which page will be chosen based on which has more objects: commits or logs.
    The longer page will allow the user to navigate through all of the objects instead
    of possibly being limited by the one with less objects.
    """
    if commit_data['count'] > log_data['count']:
        page = commit_data['page']
    else:
        page = log_data['page']
    return page

def get_log_hours(logs):
    """
    Calculates the total number of logged hours of an author for the logs header.
    """
    log_hours = 0
    for log in logs:
        log_hours += log['hours']
    return log_hours

# =================================================================================================================
# UTILITY URLS - NON DJANGO FRAMEWORK REST (for those, see api.py)

@csrf_exempt
def webhook_post(request, *args, **kwargs):
    """
    Receive an incoming webhook from something like GitHub and potentially flag a source code repo for a future scan,
    using the code in webhooks.py
    """

    if request.method != 'POST':
        return redirect('index')

    try:
        query = parse_qs(request.META['QUERY_STRING'])
        token = query.get('token', None)
        if token is not None:
            token = token[0]
        Webhooks(request, token).handle()
    except Exception:
        traceback.print_exc()
        return HttpResponseServerError("webhook processing error")

    return HttpResponse("ok", content_type="text/plain")
